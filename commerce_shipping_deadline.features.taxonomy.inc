<?php
/**
 * @file
 * commerce_shipping_deadline.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function commerce_shipping_deadline_taxonomy_default_vocabularies() {
  return array(
    'commerce_shipping_deadlines' => array(
      'name' => 'Shipping Deadlines',
      'machine_name' => 'commerce_shipping_deadlines',
      'description' => 'Vocabulary for handling different shipping deadlines of commerce products',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
