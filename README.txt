
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation and usage


INTRODUCTION
------------

This module provides order shipping deadline handling functionality to Drupal Commerce.

The module lets you have shipping deadline (in days) setting on each product. 
When a user adds a product (with valid shipping deadline) to the cart, the actual 
shipping deadline is calculated by adding the given deadline days to the current date. 
The deadline is calculated for each line item, and for the order. The order gets 
the latest date if it contains products with different deadlines. The deadline 
field can be displayed in shopping cart or order details views. Once the order 
process is completed, the dates are re-calculated.

The module provides the following:

 - "Shipping Deadlines" taxonomy vocabulary that can be attached to any Product type
 - "Shipping Deadline in days" (int) field attached to "Shipping Deadlines" vocabulary
 - "Shipping Deadline" (date) field attached to Line Item and Order entities
 - Rule and Rule Component which sets shipping deadline to line item and order


Why taxonomy vocabulary instead of a single field on the product?

This behavior adds the ability to:

 - Set a custom text for each deadline settings (ie. 14 days can have "2 Weeks" as title)
 - Terms are translatable
 - Each term have its own page, so you can provide additional information on a specific deadline
 - Also you can extend it with additional fields

INSTALLATION AND USAGE
----------------------

1. Enable the module.
2. Add 'field_shipping_deadline_ref' field to product (variation) types on which 
   you want to use shipping deadline handling.
3. Add terms to the "Shipping Deadlines" vocabulary. 
   Examples:
   [title]        [Shipping Deadline in days]
   1-3 days       3
   in 1 week      7
4. Set desired "Shipping Deadline" on products
