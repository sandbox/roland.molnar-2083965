<?php
/**
 * @file
 * commerce_shipping_deadline.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_shipping_deadline_default_rules_configuration() {
  $items = array();
  $items['rules_commerce_shipping_deadline_calculate'] = entity_import('rules_config', '{ "rules_commerce_shipping_deadline_calculate" : {
      "LABEL" : "Calculate and set shipping deadline on line item",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "data_is" : { "data" : [ "commerce-line-item:type" ], "value" : "product" } },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item:commerce-product" ],
            "field" : "field_shipping_deadline_ref"
          }
        },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item:commerce-product:field-shipping-deadline-ref" ],
            "field" : "field_shipping_deadline_days"
          }
        },
        { "data_is" : {
            "data" : [
              "commerce-line-item:commerce-product:field-shipping-deadline-ref:field-shipping-deadline-days"
            ],
            "op" : "\\u003E",
            "value" : 1
          }
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-line-item:field-shipping-deadline-date" ],
            "value" : "now"
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:field-shipping-deadline-date" ],
              "op" : "+",
              "input_2" : [
                "commerce-line-item:commerce-product:field-shipping-deadline-ref:field-shipping-deadline-days"
              ]
            },
            "PROVIDE" : { "result" : { "result" : "Calculation result" } }
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:field-shipping-deadline-date" ],
            "value" : [ "result" ]
          }
        },
        { "component_rules_commerce_shipping_deadline_set_max" : { "line_item" : [ "commerce-line-item" ] } }
      ]
    }
  }');
  $items['rules_commerce_shipping_deadline_clear_order'] = entity_import('rules_config', '{ "rules_commerce_shipping_deadline_clear_order" : {
      "LABEL" : "Clear shipping deadline on the order",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "commerce_order" : { "label" : "Order", "type" : "commerce_order" } },
      "IF" : [
        { "data_is" : { "data" : [ "commerce-order:type" ], "value" : "commerce_order" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-order:field-shipping-deadline-date" ],
            "value" : "now"
          }
        }
      ]
    }
  }');
  $items['rules_commerce_shipping_deadline_set'] = entity_import('rules_config', '{ "rules_commerce_shipping_deadline_set" : {
      "LABEL" : "Calculate and set shipping deadline on order and line items",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_checkout", "entity" ],
      "ON" : [ "commerce_checkout_complete", "commerce_order_update" ],
      "DO" : [
        { "component_rules_commerce_shipping_deadline_clear_order" : { "commerce_order" : [ "commerce_order" ] } },
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "line_item" : "Line Item" },
            "DO" : [
              { "component_rules_commerce_shipping_deadline_calculate" : { "commerce_line_item" : [ "line-item" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_commerce_shipping_deadline_set_max'] = entity_import('rules_config', '{ "rules_commerce_shipping_deadline_set_max" : {
      "LABEL" : "Set the latest deadline date to order",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "data_is" : { "data" : [ "line-item:type" ], "value" : "product" } },
        { "data_is" : { "data" : [ "line-item:order:type" ], "value" : "commerce_order" } },
        { "data_is" : {
            "data" : [ "line-item:order:field-shipping-deadline-date" ],
            "op" : "\\u003C",
            "value" : [ "line-item:field-shipping-deadline-date" ]
          }
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "line-item:order:field-shipping-deadline-date" ],
            "value" : [ "line-item:field-shipping-deadline-date" ]
          }
        }
      ]
    }
  }');
  return $items;
}
